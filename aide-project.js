// This file will be executed by Aide at application startup.
// All configuration should be located below.

import Aide from 'aide-api'
import TestPage from './pages/TestPage'
import CustomPageElement from './page-elements/CustomPageElement'
import TestPageExtension from './extensions/TestPageExtension'

const project = new Aide.Project()
const pageRegistry = project.getPageRegistry()
const pageElementsRegistry = project.getPageElementsRegistry()
const pageExtensionsRegistry = project.getPageExtensionsRegistry()

// Register custom page types.
pageRegistry.register('TEST_PAGE', TestPage)

// Register extensions.
pageExtensionsRegistry.register('TEST_PAGE', TestPageExtension)

// Register custom page elements.
pageElementsRegistry.register('CUSTOM_PAGE_ELEMENT', CustomPageElement)

window.Aide.setupProject(project)