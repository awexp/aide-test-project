export default function() {
    let customElement = this.findElementById('custom-page-element')
    let header = this.findElementById('page-header')
    {
        header.setMetadata({ title: 'abc321321' })
        header.setProps({
            onClick: () => {
                let nextActive = !customElement.state.isActive

                customElement.setState({
                    isActive: nextActive,
                    text: nextActive ? 'active' : 'not active'
                })
            }
        })
    }

    /*this.getPageStore().listen('test-form', values => {
        if (values.SomeProperty === 'abc') {

        }
    })*/
}

/*
export default class TestPageExtension extends Aide.PageExtension {
    load() {
        let header = this.findElementById('page-header')
            header.setMetadata({ title: 'abc' })
    }
}
*/
