import Aide from 'aide-api'
import React from 'react'

export default class CustomPageElement extends Aide.PageElement {
    render() {
        return (
            <div style={{margin: '1rem 0'}}>
                CustomPageElement is {this.state.text}.
            </div>
        )
    }
}