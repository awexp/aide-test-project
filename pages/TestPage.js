import Aide from 'aide-api'
import React from 'react'

/**
 * Example Aide page implementation.
 */
export default class TestPage extends Aide.Page {
    /** Specifies API endpoint that will be used to fetch details of the page. */
    endpoint = '/test-page'

    getDetailsRequestParams() {
        return {
            testParam: 'testValue'
        }
    }
}