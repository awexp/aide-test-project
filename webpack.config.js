const path = require('path')

module.exports = {
    mode: 'development',
    entry: './aide-project.js',
    output: {
        filename: 'test-project.bundle.js',
        path: path.resolve('dist'),
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            babelrc: true,
                        },
                    },
                ]
            },
        ],
    },
}